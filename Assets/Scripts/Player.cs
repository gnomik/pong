﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Player : MonoBehaviour
{
    public float movementSpeed = 100f;
    Rigidbody2D body;
    SpriteRenderer spriteRenderer;
    Vector2 movement;

    Vector2 startScale;

    BoxCollider2D boxCollider;

    public bool isRightPlayer;

    public bool isRamming;

    void Awake()
    {
        body = GetComponent<Rigidbody2D>();
        startScale = transform.localScale;
        spriteRenderer = GetComponent<SpriteRenderer>();
        boxCollider = GetComponent<BoxCollider2D>();
    }

    void Start()
    {
        
    }

    void OnEnable()
    {
        Ball.BallOut += OnBallOut;
    }

    void OnDisable()
    {
        Ball.BallOut -= OnBallOut;
    }

    // Update is called once per frame
    void Update()
    {        
        if(Mathf.Abs(transform.position.y) > Camera.main.orthographicSize - boxCollider.bounds.extents.y && Mathf.Sign(movement.y) == Mathf.Sign(transform.position.y))
        {
            movement = Vector2.zero;
        }

        body.MovePosition((Vector2)transform.position + movement * movementSpeed * Time.deltaTime);
        Inputs();
    }

    void Inputs()
    {
        if(isRightPlayer)
        {
            movement.y = Input.GetAxisRaw("VerticalRight");
        } else {
            movement.y = Input.GetAxisRaw("Vertical");
        }
        
        isRamming = Input.GetButton("Submit");
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.CompareTag("Ball"))   
        {
            transform.DOPunchScale(Vector3.one * .5f, .5f).OnComplete(() => {
                transform.localScale = Vector3.one;
            });        
        } else {
            if(isRamming)
            {
                Vector3 angles = Camera.main.transform.rotation.eulerAngles + (Vector3.forward * 10f * (transform.position.y > 0 ? 1f : -1f) * (isRightPlayer ? -1f : 1f));
                Camera.main.transform.DORotate(angles, .3f);
                Camera.main.DOShakePosition(.3f);
            }
        }
        
    }

    void OnBallOut(bool isBallOutOnRight)
    {
        
    }
}
