﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Ball : MonoBehaviour
{
    Rigidbody2D body;
    Vector2 direction;
    float magnitude = 5f;
    int hitCount = 0;
    int outCount = 0;
    ParticleSystem hitParticles;
    ParticleSystem trailParticles;
    ParticleSystem.MainModule trailParticlesMain;
    SpriteRenderer spriteRenderer;
    public static System.Action BallHit;
    public static System.Action<bool> BallOut;

    AudioSource audioSource;

    public AudioClip[] hitClips;

    public AudioClip ballOutClip;

    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        hitParticles = transform.Find("HitParticles").GetComponent<ParticleSystem>();
        trailParticles = transform.Find("TrailParticles").GetComponent<ParticleSystem>();
        trailParticlesMain = trailParticles.main;
        spriteRenderer = GetComponent<SpriteRenderer>();
        audioSource = GetComponent<AudioSource>();
        Reset();
    }

    void Reset()
    {
        direction.x = Random.Range(0, 2) * 2 - 1;
        direction.y = Random.Range(0, 2) * 2 - 1;
        transform.position = Vector3.zero;
        spriteRenderer.color = Color.white;        
        trailParticlesMain.startColor = spriteRenderer.color;
        hitCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //body.velocity = direction * Time.deltaTime * magnitude;        
    }

    void FixedUpdate()
    {
        body.MovePosition((Vector2)transform.position + (direction * Time.fixedDeltaTime * (magnitude + .30f * hitCount)));
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.contacts[0].normal.x != 0)
        {
            direction.x *= -1f;
        } else {
            direction.y *= -1f;
        }

        BallHit?.Invoke();
        Camera.main.DOShakePosition(.25f, .5f);
        transform.DOShakeScale(.5f, 1f, 10).OnComplete(() => {
            transform.localScale = Vector3.one;
        });
        hitParticles.Play();
        hitCount++;        

        spriteRenderer.color = new Color(1f, 1f - (hitCount * .05f), 1f - (hitCount * .1f));
        trailParticlesMain.startColor = spriteRenderer.color;

        audioSource.PlayOneShot(hitClips[Random.Range(0, hitClips.Length)]);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.CompareTag("Borders"))
        {
            BallOut?.Invoke(transform.position.x > 0);
            outCount++;
            trailParticlesMain.startColor = Color.blue;
            audioSource.PlayOneShot(ballOutClip);
            if(outCount == 5)
            {
                //GameObject newBallGameObject = GameObject.Instantiate(gameObject, Vector3.zero, Quaternion.identity);
            }

            Reset();
        }
    }
}
