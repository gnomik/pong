﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class GameCamera : MonoBehaviour
{
    
    void OnEnable()
    {
        Ball.BallHit += OnBallHit;
    }

    void OnDisable()
    {
        Ball.BallHit -= OnBallHit;
    }

    void OnBallHit()
    {
        //Camera.main.DOShakePosition(.25f, .5f);
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
