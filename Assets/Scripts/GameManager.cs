﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
public class GameManager : MonoBehaviour
{
    int leftPlayerScore = 0;
    int rightPlayerScore = 0;

    public TextMeshProUGUI leftPlayerScoreText;
    public TextMeshProUGUI rightPlayerScoreText;

    void OnAwake()
    {
        
    }

    void OnEnable()
    {
        Ball.BallOut += OnBallOut;
    }

    void OnDisable()
    {
        Ball.BallOut -= OnBallOut;
    }

    void OnBallOut(bool outOnRightSide)
    {
        if(outOnRightSide)
        {
            leftPlayerScore++;
            leftPlayerScoreText.text = leftPlayerScore.ToString();
            leftPlayerScoreText.GetComponent<RectTransform>().DOPunchScale(Vector3.one * .5f, .1f);
        } else {
            rightPlayerScore++;
            rightPlayerScoreText.text = rightPlayerScore.ToString();
            rightPlayerScoreText.GetComponent<RectTransform>().DOPunchScale(Vector3.one * .5f, 1f);
        }
    }
}
