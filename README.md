# Pong


## Créer un nouveau projet
### Différences 2D / 3D
- Caméra orthographique
- Importation des images en sprites automatiquement
- Pas de Skybox

### Caméra principale
- Mode ortho
- Solid color

## L'éditeur

### Hiérarchie
- Parenting, influence sur scale, position et rotation
- Filesystem

### Inspecteur
- Tag, Layer, Icône
- Liste des composants

### Scene view
- 2D, 3D


### Game view
- Stats
- Maximize on play


### Console

## Créer un joueur

### Les composants

#### Principes de base

#### Rigidbody 2D
[Lien vers la documentation officielle](https://docs.unity3d.com/ScriptReference/Rigidbody2D.html)
- Static, Dynamic, Kinematic
- Gravity scale

#### Box collider 2D
[Lien vers la documentation officielle](https://docs.unity3d.com/ScriptReference/BoxCollider2D.html)
- Physics material
- Is trigger

#### Sprite renderer
- Sorting layer
- Sprite type

### Importer un sprite

- Créer un dossier adéquat
- Vérifier les options d'importations
- Pixel per units

## Les scripts

### Monobehaviour messages par défaut 
[Lien vers la documentation officielle](https://docs.unity3d.com/ScriptReference/MonoBehaviour.html)

- Start, Awake
- Update
- Collision
- BecameInvisible

### Exposer ses variables dans l'inspecteur
- Petit piège concernant les variables publiques

## Inputs

- Émulation de l'analogie du clavier
- GetAxis
- Configuration 


## L'inspecteur

- Les tags


## Collisions
[Lien vers la documentation officielle](https://docs.unity3d.com/Manual/CollidersOverview.html)


## Librairies utilisées
- DOTween
- TextMeshPro

## Outils
- Bfxr